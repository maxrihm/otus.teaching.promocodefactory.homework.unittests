﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotFound_ReturnsNotFound()
        {
            // Arrange
            var fixture = new Fixture();
            var partnerId = Guid.NewGuid();
            var request = fixture.Create<SetPartnerPromoCodeLimitRequest>(); // create a request object
            var mockPartnerRepo = new Mock<IRepository<Partner>>();
            mockPartnerRepo.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync((Partner)null);
            var controller = new PartnersController(mockPartnerRepo.Object);

            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request); // pass the request object

            // Assert
            result.Should().BeOfType<NotFoundResult>();
        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsInactive_ReturnsBadRequest()
        {
            // Arrange
            var fixture = new Fixture();
            var partnerId = Guid.NewGuid();
            var request = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            var inactivePartner = new Partner
            {
                Id = partnerId,
                IsActive = false
            };

            var mockPartnerRepo = new Mock<IRepository<Partner>>();
            mockPartnerRepo.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(inactivePartner);
            var controller = new PartnersController(mockPartnerRepo.Object);

            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_NewLimitSet_NumberIssuedPromoCodesReset()
        {
            // Arrange
            var fixture = new Fixture();
            var partnerId = Guid.NewGuid();
            var request = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            var activeLimit = new PartnerPromoCodeLimit { CancelDate = null }; // an active limit

            var partnerWithIssuedPromoCodes = new Partner
            {
                Id = partnerId,
                IsActive = true,
                NumberIssuedPromoCodes = 5, // assuming partner has already issued 5 promo codes
                PartnerLimits = new List<PartnerPromoCodeLimit> { activeLimit } // initialize the PartnerLimits collection with the active limit
            };

            var mockPartnerRepo = new Mock<IRepository<Partner>>();
            mockPartnerRepo.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partnerWithIssuedPromoCodes);
            var controller = new PartnersController(mockPartnerRepo.Object);

            // Act
            await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            var updatedPartner = await mockPartnerRepo.Object.GetByIdAsync(partnerId);
            updatedPartner.NumberIssuedPromoCodes.Should().Be(0);
        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_NewLimitSet_PreviousLimitCancelled()
        {
            // Arrange
            var fixture = new Fixture();
            var partnerId = Guid.NewGuid();
            var request = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            var activeLimit = new PartnerPromoCodeLimit { CancelDate = null }; // an active limit

            var partnerWithActiveLimit = new Partner
            {
                Id = partnerId,
                IsActive = true,
                NumberIssuedPromoCodes = 5, // assuming partner has already issued 5 promo codes
                PartnerLimits = new List<PartnerPromoCodeLimit> { activeLimit } // initialize the PartnerLimits collection with the active limit
            };

            var mockPartnerRepo = new Mock<IRepository<Partner>>();
            mockPartnerRepo.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(partnerWithActiveLimit);
            var controller = new PartnersController(mockPartnerRepo.Object);

            // Act
            await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            activeLimit.CancelDate.Should().NotBeNull();
        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_NewLimitSet_LimitSavedInDatabase()
        {
            // Arrange
            var fixture = new Fixture();
            var partnerId = Guid.NewGuid();
            var request = fixture.Create<SetPartnerPromoCodeLimitRequest>();

            var activePartner = new Partner
            {
                Id = partnerId,
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>() // initialize the PartnerLimits collection
            };

            var mockPartnerRepo = new Mock<IRepository<Partner>>();
            mockPartnerRepo.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(activePartner);
            var controller = new PartnersController(mockPartnerRepo.Object);

            // Act
            await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            mockPartnerRepo.Verify(repo => repo.UpdateAsync(It.Is<Partner>(
                p => p.Id == partnerId &&
                     p.PartnerLimits.Any(l => l.Limit == request.Limit))), Times.Once);
        }
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_LimitIsZeroOrNegative_ReturnsBadRequest()
        {
            // Arrange
            var fixture = new Fixture();
            var partnerId = Guid.NewGuid();

            var request = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 0 // or any negative number
            };

            var activePartner = new Partner
            {
                Id = partnerId,
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>() // initialize the PartnerLimits collection
            };

            var mockPartnerRepo = new Mock<IRepository<Partner>>();
            mockPartnerRepo.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(activePartner);
            var controller = new PartnersController(mockPartnerRepo.Object);

            // Act
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerId, request);

            // Assert
            result.Should().BeOfType<BadRequestObjectResult>();
        }








    }
}